/*
 * /hw1/xcipher.c
 * 
 * This is a user-level program to encrypt a file using a system call sys_xcrypt *
 * Copyright (C) Sai Sandeep
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include <string.h>
#include "xcipher.h"

int main (int argc, char **argv)
{
	int eflag = 0, dflag = 0, pflag = 0, cflag = 0, hflag = 0;
	char *keybuf = NULL, *infile = NULL, *outfile = NULL;
	char ch;
	int i=0,c=0,len=0;
	MD5_CTX context;
	unsigned char block[16];
	long l;
	opterr = 0;
   
	while ( (c = getopt(argc,argv, "p:edc:h")) != -1)
	switch (c)
	{
        case 'p':
           pflag = 1;i++;
           keybuf=optarg;
           break;
        case 'e':
           eflag = 1;i++;
           break;
        case 'd':
           dflag = 1;i++;
           break;
        case 'c':
           cflag = 1;i++;
           break;
        case 'h':
           hflag = 1;i++;
           break;
        case '?':
           if (optopt == 'p')
              fprintf(stderr, "Option -p requires an argument.\n");
           else
           fprintf(stderr, "Unknown option '%c'. \n", optopt);
           i++;
           return -1;
        default:
           abort();
      }
	if ( hflag == 1)
	{
		printf(" print in xcipher -p password -e infile outfile");
		return 0;
	}
   
/*	if ( argc - i != 4 )
	{
	printf("Invalid number of Arguments %d\n", argc-i);
	return -1;   
	}*/
	infile = argv[argc-2];
	outfile = argv[argc-1];


	if( (eflag == 1 && dflag == 1) || (eflag == 0 && dflag == 0) )
	{
	     printf(" You should provide one of -e or -d options\n");
	     return -1;
	}
   
	i=0;
	while( (ch = keybuf[i]) )
	{
		if ( ch == '\0')
	        break;
		i++;
	}
	len=i;
	if(len<7)
	{
	printf("Invaled length of password. It hould be minimum of 7 chars");
	return -1;
	}
/* Creating Hash Using MD5 */

	MD5_Init(&context);
	MD5_Update(&context,keybuf,len);
	MD5_Final(block,&context);

/* Calling system call based on encryption or decryption */

	len=16;
	if ( eflag == 1)
	  l = sys_xcrypt(infile, outfile, block, len, 0);
	else
	  l = sys_xcrypt(infile, outfile, block, len, 1);
if(l != 0)
 perror("Error occured due to\n");
  return 0;
}




