/* System call to encrypt a file 
 * 
 * This is not the actual implementation of the required function. 
 * This enables us to add the functionality using modules using the pointer
 *
*/

#include <linux/syscalls.h>
#include <linux/module.h>

long (*foofxn)(char *file1, char *file2, void *buf, int len, int flags)=NULL;      
EXPORT_SYMBOL(foofxn);           /* Declaring a function pointer as global */

/* If pointer is refering to any other function, that will be called */
asmlinkage long sys_xcrypt(char *infile, char *outfile, void *buf, int len, int flags)
{
if(foofxn == NULL)
 return -ENOSYS;
return foofxn(infile, outfile, buf, len, flags);
}
