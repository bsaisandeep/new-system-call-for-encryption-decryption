/*
 * /hw1/xcipher.h
 * 
 * This is a header file to user-level program to encrypt a file using a system call sys_xcrypt 
 * Copyright (C) Sai Sandeep
 */

#include <unistd.h>

#define __NR_sys_xcrypt 350

long sys_xcrypt(const char *file1, const char *file2, void *buf, int len, int flags)
{
return syscall(__NR_sys_xcrypt,file1,file2,buf,len,flags);
}

