obj-y :=xcrypt.o
obj-m :=sys_xcrypt.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	gcc -Wall -Werror xcipher.c -o xcipher -lssl
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
