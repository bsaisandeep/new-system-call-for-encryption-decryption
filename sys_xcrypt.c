#include <linux/crypto.h>
#include <linux/module.h>
#include <linux/unistd.h>
#include <linux/file.h>
#include <linux/fcntl.h>
#include <linux/uio.h>
#include <linux/fsnotify.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/err.h>
#include <linux/scatterlist.h>
#include <linux/init.h>

#define AES_KEY_SIZE 16
#define CEPH_AES_IV "cephsageyudagreg"
#define MAX 4096

extern long (*foofxn)(char *file1, char *file2, void *buf, int len, int flags);


static struct crypto_blkcipher *ceph_crypto_alloc_cipher(void)
{
	return crypto_alloc_blkcipher("cbc(aes)", 0, CRYPTO_ALG_ASYNC);
}


static const u8 *aes_iv = (u8 *)CEPH_AES_IV;

/* Encryption using AES CBC mode Algorithm */

int encrypt(const void *key,int len, void *dest,size_t *len1, const void *src1, size_t len2)
{

	struct scatterlist sg_in[2], sg_out[1];
	struct crypto_blkcipher *tfm = ceph_crypto_alloc_cipher();
	struct blkcipher_desc desc = { .tfm = tfm, .flags = 0};
	int ret,ivsize;
	void *iv;
	size_t zero_padding = (0x10 - (len2 & 0x0f));
	char pad[16];
	if (IS_ERR(tfm))
	  return PTR_ERR(tfm);

	memset(pad, zero_padding, zero_padding);
	*len1 = len2 + zero_padding;
	crypto_blkcipher_setkey((void *)tfm, key, len);
	sg_init_table(sg_in,2);
	sg_set_buf(&sg_in[0], src1, len2);
	sg_set_buf(&sg_in[1], pad, zero_padding);
	sg_init_table(sg_out,1);
	sg_set_buf(sg_out,dest,*len1);
	iv = crypto_blkcipher_crt(tfm)->iv;
	ivsize = crypto_blkcipher_ivsize(tfm);
	memcpy(iv,aes_iv,ivsize);
  ret = crypto_blkcipher_encrypt(&desc, sg_out, sg_in, len2+ zero_padding);
	crypto_free_blkcipher(tfm);
	if(ret<0)
	  printk("error");
	
return 0;

}

/* Decryption using AES CBC mode Algorithm */

int decrypt(const void *key,int len, void *dest,size_t *len1, const void *src1, size_t len2)
{

	struct scatterlist sg_in[1], sg_out[2];
	struct crypto_blkcipher *tfm = ceph_crypto_alloc_cipher();
	struct blkcipher_desc desc = { .tfm = tfm };
	int ret,ivsize;
	void *iv;
	char pad[16];
	int last_byte;
	if (IS_ERR(tfm))
	 return PTR_ERR(tfm);

	crypto_blkcipher_setkey((void *)tfm, key, len);
	sg_init_table(sg_in,1);
	sg_init_table(sg_out,2);
	sg_set_buf(sg_in,src1,len2);
	sg_set_buf(&sg_out[0],dest,*len1);
	sg_set_buf(&sg_out[1],pad,sizeof(pad));

	iv = crypto_blkcipher_crt(tfm)->iv;
	ivsize = crypto_blkcipher_ivsize(tfm);
	memcpy(iv,aes_iv,ivsize);
	ret = crypto_blkcipher_decrypt(&desc, sg_out, sg_in, len2);
	crypto_free_blkcipher(tfm);
	if(ret<0)
	  printk("error");

	if(len2<=*len1)
	  last_byte = ( (char *)dest)[len2-1];
	else
	  last_byte = pad[len2- *len1 - 1];
	if (last_byte <=16 && len2>=last_byte)
	{
	 *len1 = len2 - last_byte;
	}

return 0;

}

/* This function reads the data from a file, sends it for encryption or decryption and then store the resultant data into another file which is specified by the user. */

long read(char *filename,char *filewrite, void *buf, int len, int flag)
{
	int bytes=MAX,err=0,err1=1,size=0,count=0,k,i=MAX,j=MAX,pad,check=0,l=16;
	int *p=&i,*p1=&j;
	char *cmp1=kmalloc(16*sizeof(unsigned char),GFP_KERNEL);
	struct inode *inode;
	char padding='0';
	char *Kfile = kmalloc((MAX)*sizeof(char),GFP_KERNEL);
	u8 *kwbuff = kmalloc((MAX)*sizeof(unsigned char),GFP_KERNEL);

	mm_segment_t oldfs;
	struct file *filp, *filpw;
	memset(cmp1,'\0',len); //copying the buffer into a char variable
	memcpy(cmp1,(char *)buf,len);

/* open a file in readonly mode only if it exists */

	filp = filp_open(filename,O_EXCL|O_RDONLY,0);
	if(!filp || IS_ERR(filp))
	{err1=2;
	  printk("read error %d\n", (int)PTR_ERR(filp));
	   goto l3;
	}

   
	if (!filp->f_op->read)
	{err1=2;
	   printk("read file pointer error");
	   goto l3;
	}

	inode = filp->f_path.dentry->d_inode;

/* open a file in write mode, only if it doesn't exist with same permissions
as of actual file */

	filpw = filp_open(filewrite,O_EXCL|O_CREAT,inode->i_mode);
	if(!filp || IS_ERR(filpw))
	{err1=3;
	   printk("write error%d\n", (int)PTR_ERR(filpw));
	   goto l2;
	}
 
	if(!filpw->f_op->write)
	{err1=8;
	   goto l2;
	}
/* Null checks for buffers */ 
	if (Kfile == NULL || kwbuff == NULL || cmp1 == NULL)
	{
	   err1=8;
	   goto l3;      
	}


	size=(int)(inode->i_size);
	count=size%MAX;
	size=size/MAX;
	if(count!=0)
	   size++;

	filp->f_pos = 0;
	filpw->f_pos = 0;
	oldfs = get_fs();
	set_fs(get_ds());
	if(len!=16)
	{ err1=9;
          goto l1;
	  
	}
     
	k=0;

/* if encryption, the below code executes */

	if(flag==0)
 	{
		if (count != 0)
		   padding='1';
     
	  err= encrypt(buf,len, kwbuff, p, cmp1,len);
	  filpw->f_op->write(filpw,kwbuff,len, &filpw->f_pos);
	  filpw->f_op->write(filpw,&padding,1,&filpw->f_pos);
	  while ( k<size )
	  { k++;
	     bytes = filp->f_op->read(filp,Kfile,MAX, &filp->f_pos);
	     pad=bytes;

/* Padding of zero's */

	     if( bytes%16 != 0 ){
	          for(count=bytes%16;count<16-1;count++)
	          {
	            Kfile[bytes++]='0';
        	  } 
 	     Kfile[bytes++]=pad%16;
      
      	     }
	     err= encrypt(buf,len, kwbuff, p, Kfile,MAX);
	     filpw->f_op->write(filpw,kwbuff,bytes, &filpw->f_pos);
	   }

	 }

/* For Decryption, the below code executes */

	if(flag==1)
	{ k=0;
	p=&l;
	bytes = filp->f_op->read(filp,kwbuff,len, &filp->f_pos);
	err= decrypt(buf,len, Kfile, p, kwbuff,len);
	for (count=0;count<len;count++)
	{
	        if( Kfile[count] != cmp1[count] )
	             check = 1;
	}	
/* checking if key is valid or not */
	if ( check == 1 )
	{    err1=9;
	     printk("invalid key");
	     goto l1;
	}
/* Read data after the preamble */

	filp->f_pos = len+1;
	bytes = MAX;
	while ( bytes == MAX )
	{ k++;
		bytes = filp->f_op->read(filp,kwbuff,MAX, &filp->f_pos);
		err= decrypt(buf,len, Kfile, p1, kwbuff,MAX);
		if ( bytes != MAX)
		       bytes=bytes - 16 + (int)Kfile[bytes-1];
     
		filpw->f_op->write(filpw,Kfile,bytes, &filpw->f_pos);
	}  

	}
	set_fs(oldfs);
 
	err1=0;
l1:     filp_close(filpw,NULL);
l2:     filp_close(filp,NULL);
l3:
	if(Kfile != NULL) 
	kfree(Kfile);
	if(kwbuff != NULL) 
	kfree(kwbuff);
	if(cmp1 != NULL) 
	kfree(cmp1);
        if (err1 == 3)
	return -EEXIST;
	if(err1 == 9)
        return -EINVAL;
        if(err1 == 8) 
	return -EBADF;
        if (err1 !=0)
   	return -EINVAL;
 return 0;

} 



/* Init Module */


int mod_init(void)
{
foofxn=read;
printk("module loaded");
return 0;
}

/* Exit Module */

void mod_exit(void)
{
printk("module exit");
}

module_init(mod_init);
module_exit(mod_exit);
MODULE_LICENSE("GPL");


